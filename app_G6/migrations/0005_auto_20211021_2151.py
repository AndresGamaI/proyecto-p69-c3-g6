# Generated by Django 3.2.8 on 2021-10-22 02:51

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app_G6', '0004_pez_url'),
    ]

    operations = [
        migrations.AlterField(
            model_name='pez',
            name='agua',
            field=models.CharField(max_length=100),
        ),
        migrations.AlterField(
            model_name='pez',
            name='url',
            field=models.TextField(default='not found'),
        ),
    ]

from rest_framework.response import Response
from rest_framework.views import APIView
from app_G6.models.ave import Ave
from app_G6.serializers.aveSerializer import AveSerializer


class AveAPIView (APIView):
  def get(self,request):
    aves=Ave.objects.all()
    aves_serializer=AveSerializer(aves,many=True)
    return Response(aves_serializer.data)

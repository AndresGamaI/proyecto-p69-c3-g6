from django.urls import path
from app_G6.api.pezApi import PezAPIView
from app_G6.api.aveApi import AveAPIView

urlpatterns=[
  
  path("pez/",PezAPIView.as_view(),name="pez_api"),
  path("ave/",AveAPIView.as_view(),name="ave_api")
]

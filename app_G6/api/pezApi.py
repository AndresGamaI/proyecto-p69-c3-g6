from rest_framework.response import Response
from rest_framework.views import APIView
from app_G6.models.pez import Pez
from app_G6.serializers.getAllPeces import PezSerializer


class PezAPIView (APIView):
  def get(self,request):
    peces=Pez.objects.all()
    peces_serializer=PezSerializer(peces,many=True)
    return Response(peces_serializer.data)


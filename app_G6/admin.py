from django.contrib import admin
from .models.user import User
from .models.account import Account
from .models.pez import Pez
from .models.categoria import category
from .models.ciudad import city
from .models.lugar import lugar
from .models.ave import Ave

admin.site.register(User)
admin.site.register(Account)
admin.site.register(Pez)
admin.site.register(category)
admin.site.register(city)
admin.site.register(lugar)
admin.site.register(Ave)


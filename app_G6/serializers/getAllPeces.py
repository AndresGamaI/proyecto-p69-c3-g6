from django.db.models import fields
from rest_framework import serializers
from app_G6.models.pez import Pez

class PezSerializer (serializers.ModelSerializer):
  class Meta:
    model=Pez
    fields="__all__"
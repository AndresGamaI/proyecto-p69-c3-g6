from django.db.models import fields
from rest_framework import serializers
from app_G6.models.ave import Ave

class AveSerializer (serializers.ModelSerializer):
  class Meta:
    model=Ave
    fields="__all__"
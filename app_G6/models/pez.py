from django.db import models

class Pez(models.Model):
    nombre=models.CharField(max_length=20)
    id = models.AutoField(primary_key=True)
    tamaño = models.FloatField()
    promedio_vida = models.CharField(max_length=20)
    velocidad = models.IntegerField()
    agua = models.CharField(max_length=100)
    url=models.TextField(default="not found")
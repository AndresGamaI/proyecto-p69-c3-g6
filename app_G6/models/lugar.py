from django.db import models
from .categoria import category
from .ciudad import city

class lugar(models.Model):
    """
    lugar para clasificar los diferentes ciudades donde se visita 
    """
    id = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=200)
    category = models.ForeignKey(category, related_name='category', verbose_name="Categorias", on_delete=models.CASCADE)
    ciudad = models.ForeignKey(city, related_name='city' , verbose_name="Ciudad", on_delete=models.CASCADE)
    comentario = models.TextField()
    is_activo = models.BooleanField(default=True)

    class Meta:
        verbose_name = 'Lugar para visitar'
        verbose_name_plural = 'Lugares visitados'

    def __str__ (self):
        return self.nombre
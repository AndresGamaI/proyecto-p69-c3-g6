from django.db import models

class category(models.Model):
    """
    categorias para clasificar los eventos de lso viajes
    """
    id = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=200)
    is_activo = models.BooleanField(default=True)

    class Meta:
        verbose_name = 'Categoria'
        verbose_name_plural = 'Categorias'

    def __str__ (self):
        return self.nombre

from django.db import models

class Ave(models.Model):
    id = models.AutoField(primary_key=True)
    altura = models.FloatField()
    nombre = models.CharField(max_length=200)
    promedio_vida = models.IntegerField()
    velocidad = models.IntegerField()
    largo_pico = models.FloatField()
    peso = models.FloatField()
    diametro_huevo = models.FloatField()

    def __str__ (self):
        return self.nombre

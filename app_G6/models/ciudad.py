from django.db import models

class city(models.Model):
    """
    Ciudad para clasificar los diferentes ciudades donde se visita 
    """
    id = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=200)
    is_activo = models.BooleanField(default=True)

    class Meta:
        verbose_name = 'Ciudad'
        verbose_name_plural = 'Ciudades'

    def __str__ (self):
        return self.nombre